<?php
/* 
* @Title: [函数集合]
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 21:03:22
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-12 17:03:20
* @Copyright: Hn7m.com
*/
/**
 * [p 打印函数]
 * @param  [type] $arr [description]
 * @return [type]      [description]
 */
function p($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}

/**
 * [go 跳转到指定页面函数]
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function go($url){
	echo
	"<script type='text/javascript'>
		window.location.href='$url';
	</script>";
}
/**
 * [C 打印并修改配置项函数]
 * @param [type] $key   [description]
 * @param [type] $value [description]
 */
function C($key = NULL, $value = NULL){
	static $arr = array();
	if(is_array($key)){
		$arr = array_merge($arr, array_change_key_case($key,CASE_UPPER));
	}
	if(is_string($key)){
		if(!is_null($value)){
			$arr[$key] = $value;
		}else{
			return isset($arr[$key]) ? $arr[$key] : NULL;
		}
	}
	if(is_null($key)){
		return $arr;
	}
}
/**
 * [halt 终止错误操作函数]
 * @param  [type] $msg [description]
 * @return [type]      [description]
 */
function halt($msg){
    if(DEBUG){
        header('Content-type:text/html;charset=utf-8');
        echo $msg;
    }else{
        include DATA_PATH . '/Tpl/404.html';
    }
    die;
}

function M($table){
	$model = new Model($table);
	return $model;
}

?>