<?php
/* 
*[框架核心公用类(被继承类)--->代码的最大公用]
* @author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 15:55:50
* @Last Modified time: 2014-03-12 17:57:55
*/

class Control extends SmartyView{

	private $var = array();

	/**
     * [success 成功和错误提示方法]
     * @param  [type] $msg [提示信息]
     * @param  [type] $url [跳转地址]
     * @return [type]      [description]
     */
    protected function success($msg,$url=NULL,$time=3){
        $url = $url ? "window.location.href='" . $url . "'": 'window.history.back(-1)';
        include DATA_PATH . '/Tpl/success.html';
        die;
    }
    protected function error($msg){
        $url = 'window.history.back(-1)';
        include DATA_PATH . '/Tpl/error.html';
        die;
    }

	/**
	 * [display 默认使用Smarty模板引擎方法,如果关闭了smarty,则使用原生的引擎方法]
	 * @param  [type] $tpl [description]
	 * @return [type]      [description]
	 */
	// 引用模版文件方法，每次对象调用这个方法都可以准确引用到对应控制器名称的模版文件
    protected function display($tpl = NULL){
        if(C('SMARTY_ON')){
            parent::display($tpl);
        }else{
        	//注意:我们Tpl里面的默认模板文件夹都是和控制器命名一样
        	// 判断是否传了参数，
            //  如果没有则使用默认的模板
            if(is_null($tpl)){
                $path = APP_TPL_PATH . "/" .CONTROL."/". METHOD . '.html';
            }else{
            	//如果传参,例如传入了ndex.html.则使用参数的模板
                $suffix = strrchr($tpl,'.');
                //有可能用户传入的文件名只是ndex没有后缀,那么给他加个后缀
                $tpl = empty($suffix) ? $tpl . '.html' : $tpl;
                $path = APP_TPL_PATH . '/' . CONTROL . '/' . $tpl;
            }
            // 判断模版文件是否存在
            if(!is_file($path)){
                header('Content-type:text/html;charset=utf-8');
                echo $tpl . '模板文件未找到! ): ';die;
            }
            // display()方法引用的模版文件中都可以用assign()方法定义的变量
            // $this->var  这里的var是上面定义了一个类属性,在第一行定义了一个为空数组的私有属性。
            // extract()方法是将assign方法定义的变量转换成$a=123;的格式，如果$value是一个数组也可以转换成:$a=array('a' => '123'); 
            // extract从数组中将变量导入到当前的符号表   
            is_array($this->var) && extract($this->var);
            // 引用所需的模版文件
            include $path;
        }
    }

    // 写的这个方法就是实现任意方法中定义一个变量，display()方法引用的模版文件中都可以用这个变量
    /**
     * [assign 默认使用Smarty的assign方法,如果关闭了Smarty,则使用原生的assign]
     * @param  [type] $var   [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    protected function assign($var,$value){
        if(C('SMARTY_ON')){
            parent::assign($var,$value);
        }else{
            $this->var[$var] = $value;
        }
    }

}
?>