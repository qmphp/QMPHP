<?php

/* 
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-17 20:28:36
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-15 23:56:03
* @Copyright: Hn7m.com
*/

class Upload{
	//上传路径
	private $path;
	//上传大小
	private $size;
	//上传允许类型
	private $type;
	//上传失败记录
	public $error;

	/**
	 * [__construct 构造函数初始化配置]
	 * @param [type] $path [上传路径]
	 * @param [type] $size [上传大小]
	 * @param [type] $type [允许类型]
	 */
	public function __construct($path = NULL, $size = NULL, $type = NULL){
		//初始化数据,初始化的目的是为了让用户传参,如果传参就用用户的参数,如果不传就用配置项的参数;
		$this->path = is_null($path) ? C('UPLOAD_PATH') : $path;
		$this->size = is_null($size) ? C('UPLOAD_SIZE') : $size;
		$this->type = is_null($type) ? C('UPLOAD_TYPE') : $type;

		// var_dump($path);
		// echo('arg1');
		// p(C('UPLOAD_PATH'));

	}
	/**
	 * [up_load 外部调用上传方法]
	 * @return [type] [description]
	 */
	public function up_load(){
		//组合数组
		$arr = $this->_reset_arr();
		//p($arr);
		//p(C());
		//定义空数组，最后返回所有信息
		$fileArr = array();
		//循环组合好的数组
		foreach($arr as $key =>$value){
			//筛选成功以后，执行上传
			//$n = $key+1;
			if($this->_filter($value)){
				//var_dump($this->_filter($value));
				//p($value);
				$fileArr[] = $this->_move($value,$key);
			}else{
				$n = $key+1;
				//echo '文件'.$n.'上传失败'.'<br/>';
				//$this->error('文件'.$n.'上传失败,错误信息:'. $this->error . '!'.'<br/>');
			}
		}
		//返回上传文件信息
		//p($fileArr);
		return $fileArr;
	}

	/**
	 * [reset_arr 组合数组]
	 * @return [type] [description]
	 */
	private function _reset_arr(){  //组合成二维数组

		//如果$_FILES不为空证明有用户提交
		if(!empty($_FILES)){

			//定义空数组，最终要把信息组合到这个空数组中
			$arr = array();
			//循环上传文件信息
			foreach ($_FILES as $v) {
				//如果$v['name']为数组，证明是多个文件
				if(is_array($v['name'])){
					//循环$v['name']或者其他$v['type']最终是为了
					//需要$key
					foreach ($v['name'] as $key => $value) {
						//组合新数组
						$arr[] = array(
							'name'		=>	$v['name'][$key],
							'type'		=>	$v['type'][$key],
							'tmp_name'	=>	$v['tmp_name'][$key],
							'error'		=>	$v['error'][$key],
							'size'		=>	$v['size'][$key]
						);
					}					
				}else{
					//否则是单文件直接压入数组
					$arr[] = $v;
				}
			}
		}else{
			$arr=array();
		}
		return $arr;

	}

	/**
	 * [_filter 筛选]
	 * @return [type] [description]
	 */
	private function _filter($arr){	
		//p($arr);
		//去掉原数组type的前缀,即去掉image/jpeg中的'image/'
		$arrInfo = pathinfo($arr['name']);
		$type = $arrInfo['extension'];
		//p($type);
		//判断文件类型是否为图片以及判断文件的大小是否超过5000000;
 		switch (true) {
 		 			case !is_uploaded_file($arr['tmp_name']):
 		 				$this->error = '不是一个上传文件';
 		 				return false;
 		 			
 		 			case !in_array($type, $this->type):
 		 				$this->error = '上传的文件类型不正确';
 		 				return false;

 		 			case $arr['size']>$this->size:
 		 				$this->error = '上传的文件大小超限';
 		 				return false;

 		 			case $arr['error'] == 1:
 		 				$this->error = '大小超过了 php.ini 中 upload_max_filesize 限制值';
 		 				return false;

 		 			case $arr['error'] == 2:
 		 				$this->error = '大小超过 HTML 表单中 MAX_FILE_SIZE 选项指定的值';
 		 				return false;

 		 			case $arr['error'] == 3:
 		 				$this->error = '文件只有部分被上传';
 		 				return false;

 		 			case $arr['error'] == 4:
 		 				$this->error = '没有文件被上传';
 		 				return false;

 		 			case $arr['error'] == 5:
 		 				$this->error = '上传文件大小为0';
 		 				return false;

 		 			default:
 		 				return true;
 		 		} 		
	}

	

	/**
	 * [_move 移动上传文件]
	 * @return [type] [description]
	 */
	private function _move($arr,$key){

		//规范路径,获得配置文件中指定的上传目标文件夹路径
		$path = $this->path;
		$path = str_replace("\\", '/', $path);
		$path = rtrim($path, '/') . '/';

		//判断上传的目标文件夹是否存在,如果不存在就创建之;
		is_dir($path) || mkdir($path,0777,true);

		//step two:给上传的文件重命名,一来便于管理,二来避免不同用户上传来的文件名相同而产生的覆盖
		//取得文件信息扩展名
		$expire = ltrim(strrchr($arr['name'], '.'),'.');
		//为了图片唯一性，给他随机数和当前的时间戳
		$filename = date('Ymd') . mt_rand(1,99) . '.' .$expire;
		//组合新的路径
		$path = $path . $filename;
		//step three:执行上传动作
		//执行上传动作，第一个临时文件，第二个目录文件
		$bool = move_uploaded_file($arr['tmp_name'], $path);

		if($bool){
			return $arr['save'] = $path;
		}else{
			return $arr['save'] = null;
		}		
	}
}
