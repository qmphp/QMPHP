<?php
/* 
* @Title:  [Smarty模板引擎处理类]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-12 13:41:22
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-13 08:53:27
* @Copyright:  [hn7m.com]
*/
require ORG_PATH . '/smarty/Smarty.class.php';

class SmartyView{
	//定义实例化保存的属性
	private $smarty = NULL;

	/**
	 * [__construct 自动运行]
	 */
	public function __construct(){
		if(!is_null($this->smarty)) return;
		//实例化Smarty对象
		$smarty = new Smarty(); 
		//模版目录
		$smarty->template_dir = APP_TPL_PATH . "/" . CONTROL . '/'; 
		//编译目录
		$smarty->compile_dir = APP_COMPILE_PATH;
		//缓存目录
		$smarty->cache_dir = APP_CACHE_PATH;
		//开始定界符
		$smarty->left_delimiter = C('SMARTY_DELIMITER_LEFT'); 
		//结束定界符
		$smarty->right_delimiter = C('SMARTY_DELIMITER_RIGHT');
		//是否缓存
		$smarty->caching = C('SMARTY_CACHE_ON');
		//缓存时间
		$smarty->cache_lifetime = C('SMARTY_CACHE_TIME');
		//保存到属性中
		$this->smarty = $smarty;
	}

	/**
	 * [display description]
	 * @return [type] [description]
	 */
	protected function display($tpl = NULL){
		$tpl = $this->get_tpl($tpl);
		$this->smarty->display($tpl, $_SERVER['REQUEST_URI']);
	}

	protected function assign($var,$value){
		$this->smarty->assign($var,$value);
	}

	/**
	 * [get_tpl 获得模板,index.html 或者 admin.htm]
	 * @param  [type] $tpl [没有路径]
	 * @return [type]      [description]
	 */
	private function get_tpl($tpl){
		if(is_null($tpl)){
            $tpl = METHOD . '.html';
        }else{
            $suffix = strrchr($tpl,'.');
            $tpl = empty($suffix) ? $tpl . '.html' : $tpl;
        }

        $tpl = APP_TPL_PATH.'/'.CONTROL.'/'.$tpl;
        //echo $tpl;
        return $tpl;
	}

	/**
	 * [is_cached 判断缓存是否开启]
	 * @return boolean [description]
	 */
	public function is_cached($tpl = NULL){
		if(!C('SMARTY_ON')) halt('请先开启smarty');
		$tpl = $this->get_tpl($tpl);
		return $this->smarty->is_cached($tpl,$_SERVER['REQUEST_URI']);
	}
	
}
?>