<?php
/* 
* @Title:  [自定义的JS文件引入方法]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-11 18:02:40
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-11 19:07:23
* @Copyright:  [hn7m.com]
*/

function smarty_function_custom_js($params, &$smarty){
    return "<script type='text/javascript' src=" . $params['file'] . "></script>";
}
?>