<?php
/* 
* @Title: [框架核心处理类]
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 19:21:42
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-15 23:09:43
* @Copyright: Hn7m.com
*/

class Qm{
	/**
	 * [run 核心类运行开始]
	 * @return [type] [description]
	 */
	public function run(){
		//1.定义框架所需常量
		self::_set_const();
		//2.创建所需的文件夹/或引入boot文件
		//检测DEBUG模式是否开启，如果开启表示还在开阶段，没开启则表示已经上线
		defined('DEBUG') || define('DEBUG', FALSE);//---->这一步的好处是，项目在上线后，不管用户是在单入口开启了DEBUG常量为FALSE模式还是直接删除了DEBUG常量，都可以引boot文件
		if(DEBUG){
			//如果开启DEBUG模式，则创建文件夹
			self::_create_dir();
			//3.载入框架所需的必要文件;
			self::_import_file();
		}else{
			//否则引入boot文件
			include TEMP_PATH .'/boot.php';
		}	
		
		//4.运行类执行run方法;
		Application::run();
	}
	/**[STEP ONE]
	 * [_set_const 定义框架所需常量,组合路径;]
	 */
	private static function _set_const(){
		$path = str_replace("\\", "/", dirname(__FILE__));
		//var_dump($path);
		//定义框架根路径
		define('QM_PATH', $path);
		//定义网站根路径
		define('ROOT_PATH', dirname(QM_PATH));
		//定义临时目录路径
		define('TEMP_PATH', ROOT_PATH.'/Temp');
		//定义上传目录路径
		define('UPLOAD_PATH', ROOT_PATH.'/UploadFile');
		//定义配置文件路径
		define('CONFIG_PATH', QM_PATH.'/Config');
		//定义框架Data路径,这个目录主要放字体文件,404页面等
		define('DATA_PATH', QM_PATH.'/Data');
		//定义框架拓展extend目录路径
		define('EXTEND_PATH', QM_PATH.'/Extend');
		//定义框架拓展extend目录下的tool路径
		define('TOOL_PATH', EXTEND_PATH.'/Tool');
		//定义框架拓展extend目录下的Org路径
		define('ORG_PATH', EXTEND_PATH.'/Org');
		

		//定义框架核心Lib目录路径
		define('LIB_PATH', QM_PATH.'/Lib');
		//定义框架核心Lib目录下的文件汇编目录Compile路径
		define('COMPILE_PATH', LIB_PATH.'/Compile');
		//定义框架核心Lib目录下的核心类目录Core路径
		define('CORE_PATH', LIB_PATH.'/Core');
		//定义框架核心Lib目录下的函数类目录Funciton路径
		define('FUNCTION_PATH', LIB_PATH.'/Function');

		//定义应用路径
		define('APP_PATH', ROOT_PATH.'/'.APP_NAME);
		//定义应用下面的Config文件夹
		define('APP_CONFIG_PATH', APP_PATH.'/Config');
		//定义应用下面的Control文件夹(放应用的控制器)
		define('APP_CONTROL_PATH',APP_PATH.'/Control');
		//定义应用下面的Tpl文件夹
		define('APP_TPL_PATH', APP_PATH.'/Tpl');
		//定义应有里面的Tpl下面的public文件夹,这个文件放js或css等
		define('APP_PUBLIC_PATH', APP_TPL_PATH.'/Public');

		//定义应用下面的Temp文件夹,放模板引擎编译和缓存目录
		define('APP_TEMP_PATH', APP_PATH.'/Temp');
		//定义Temp下面的编译目录
		define('APP_COMPILE_PATH', APP_TEMP_PATH.'/Compile');
		//定义Temp下面的缓存目录
		define('APP_CACHE_PATH', APP_TEMP_PATH.'/Cache');

		//定义一些外部变量方法常量;
		define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
		//定义POST方法常量
		define('IS_POST', REQUEST_METHOD == 'POST'?TRUE:FALSE);
		//定义GET方法常量
		define('IS_GET', REQUEST_METHOD == 'GET'?TRUE:FALSE);
		//定义ajax常量
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
            define('IS_AJAX',TRUE);
        }else{
            define('IS_AJAX',FALSE);
        }
	}

	/**[STEP TWO]
	 * [_create_dir 创建应有所需的文件夹]
	 * @return [type] [description]
	 */
	private static function _create_dir(){
		$arr=array(
			APP_CONFIG_PATH,
			APP_CONTROL_PATH,
			APP_TPL_PATH,
			APP_PUBLIC_PATH,
			APP_TEMP_PATH,
			APP_CACHE_PATH,
			APP_COMPILE_PATH,
			TEMP_PATH,
			UPLOAD_PATH
			);
		foreach ($arr as $key => $value) {
			//因为这里有第三个参数,因此会自动递归创建
			is_dir($value) || mkdir($value,0777,TRUE);
		}
	}

	/**[STEP THREE]
	 * [_import_file 载入框架所需的必要文件]
	 * @return [type] [description]
	 */
	private static function _import_file(){
		//var_dump(COMPILE_PATH);
		$file = require COMPILE_PATH.'/Compile.php';
		//var_dump($file);
		foreach ($file as $key => $value) {
			require $value;
		}

		//将所有的必要文件重新组合压入到boot.php中,目的是关闭DEBUG后,引用boot.php
		$str = '';
		foreach ($file as $key => $value) {
			$value = trim($value);
			$str .= substr(file_get_contents($value), 5, -2);
		}
		$str = "<?php" . $str;
		file_put_contents(TEMP_PATH.'/boot.php', $str) || die('access not allow');
	}

}

Qm::run();
?>
