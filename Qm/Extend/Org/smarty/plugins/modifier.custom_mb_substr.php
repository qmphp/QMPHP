<?php
/* 
* @Title:  [自定义的变量调节器]
* @Function:  [此调节器是根据原有的TRUNCATE方法改进而来,原有的方法对于汉字支持的不好.使用该方法可以很好的支持汉字的截取]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-11 16:16:41
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-11 17:31:17
* @Copyright:  [hn7m.com]
*/
/**
 * [smarty_modifier_custom_mb_substr 从字符串开始处截取某长度的字符]
 * @param  [type]  $string [源字符串]
 * @param  integer $length [默认截取40个]
 * @param  string  $etc    [默认以...结尾]
 * @return [type]          [如果源字符串长度大于默认截取的长度则按要求截取并以...结尾,反之不截]
 */
function smarty_modifier_custom_mb_substr($string, $length = 40, $etc = '...'){
    $str_leng= mb_strlen($string);
    if ($str_leng>$length) {
        return $string=mb_substr($string, 0,$length,'utf-8').$etc;
    } else {
       return $string;
    }
    
}


?>
