<?php
/* 
* @Title: [应用的框架类]
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 21:10:25
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-12 17:41:00
* @Copyright: Hn7m.com
*/

class Application{
	/**
	 * [run 应用类开始执行，此方法被Qm.php里面的类调用]
	 * @return [type] [description]
	 */
	public static function run(){
		//1.定义一些外部常量
		self::_set_url();
		//2.初始化框架
		self::_init();
		//3.创建DEMO
		self::_create_demo();
		//4.自动载入
		spl_autoload_register(array(__CLASS__,'_auto'));
		//5.应用实例化执行
		self::_app_run();
	}

	/**
	 * [_set_url 定义一些外部常量]
	 */
	private static function _set_url(){
		//p($_SERVER);
		$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
		$url = str_replace('\\', '/', $url);

		//按照常规写法或者说习惯,我们把控制器统一按照首字母大写后加Control来命名,例如IndexControl.class.php
		$control = isset($_GET['c'])?ucfirst(htmlspecialchars($_GET['c'])) :'Index';//控制器
		//控制器里面的方法
		$method = isset($_GET['m']) ? htmlspecialchars($_GET['m']) : 'index';

		//定义控制器和方法两个常量
		define('METHOD', $method);
		define('CONTROL', $control);

		//定义使用应用控制器和方法的写法常量
		define('__APP__', $url);
		define('__CONTROL__',__APP__ . '?&c=' . CONTROL);
		define('__METHOD__', __CONTROL__ . '&m=' . METHOD);
		define('__APL__', dirname(__APP__));
		define('__ROOT__', dirname(__APP__));
		define('__TPL__', __APL__.'/'.APP_NAME.'/Tpl');
		define('__PUBLIC__', __TPL__.'/Public');

	}

	/**
	 * [_init 初始化框架]
	 * @return [type] [description]
	 */
	private static function _init(){
		//定义默认时区和开启session
		date_default_timezone_set('PRC');
		session_start();

		//定义默认配置
		$sysConfig = CONFIG_PATH . '/Config.php';
		$userConfig = APP_CONFIG_PATH . '/Config.php';
		is_file($userConfig) || copy($sysConfig, $userConfig);

		//如果用户修改或者自定义了配置,那么就用用户的,如果没有就用系统默认的配置
		//因此用户的C函数要放在后面来写,便于覆盖系统配置
		C(include $sysConfig);
		C(include $userConfig);

	}

	/**
	 * [_create_demo 创建DEMO.给客户一个默认的模板,提升用户体验]
	 * @return [type] [description]
	 */
	private static function _create_demo(){
		//如果用户自定义了模板就用客户的模板.并直接return
		$file = $file = APP_CONTROL_PATH . '/IndexControl.class.php';
		if(is_file($file)) return;
		$str=<<<st
<?php
class IndexControl extends Control{
	public function index(){
		header('Content-type:text/html;charset=utf-8');
		echo '<h1>欢迎使用七米框架 :) </h1>';
	}
}
?>
st;
	file_put_contents($file, $str)	|| die('access not allow');
	}

	/**
	 * [_auto 自动加载]
	 * @param  [type] $className [description]
	 * @return [type]            [description]
	 */
	private static function _auto($className){
		//这个_auto方法是最上面run方法中spl_autoload_register自动载入的第二个参数
		if(strlen($className) > 7 && substr($className, -7) == 'Control'){
			$path = APP_CONTROL_PATH . '/' . $className . '.class.php';
		}else{
			$path = TOOL_PATH . '/' . $className . '.class.php';
		}
		if(!is_file($path)){
			header('Content-type:text/html;charset=utf-8');
			echo $className . '类未找到！):';die;
		}else{
			require $path;
		}
	}

	/**
	 * [_app_run 用new一个类来实例化对象]
	 * @return [type] [description]
	 */
	private static function _app_run(){

		$control = CONTROL . 'Control';
		$method = METHOD; 
		$obj = new $control();
		$obj->$method();
	}
}
?>