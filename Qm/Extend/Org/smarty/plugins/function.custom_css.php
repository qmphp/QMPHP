<?php
/* 
* @Title:  [自定义的CSS文件引入方法]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-11 19:08:36
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-11 19:14:03
* @Copyright:  [hn7m.com]
*/
function smarty_function_custom_css($params, &$smarty){
    return "<link rel='stylesheet' href=" . $params['file'] . " />";
}

?>