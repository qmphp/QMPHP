<?php
/* 
* @Title: 验证码类
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-19 15:03:09
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-06 17:07:42
* @Copyright: Hn7m.com
*/


class Creat_code{

	private $CODE_LEN;
	private $CODE_SIZE;
	private $CODE_COLOR;
	private $CODE_BGCOLOR;
	private $CODE_WIDTH;
	private $CODE_HEIGHT;
	private $CODE_FONTFILE;
	private $CODE_SEEK;
	private $img;

	/**
	 * [__construct 构造函数初始化数据,初始化的目的是为了让用户可以自定义生成的验证码样式,如果传参就用用户的参数,如果不传就用配置项的参数;]
	 * @param [type] $CODE_LEN      [验证码长度]
	 * @param [type] $CODE_WIDTH    [验证码图片宽度]
	 * @param [type] $CODE_HEIGHT   [验证码图片高度]
	 * @param [type] $CODE_BGCOLOR  [验证码图片背景颜色]
	 * @param [type] $CODE_COLOR    [验证码文字颜色]
	 * @param [type] $CODE_SIZE     [验证码大小]
	 * @param [type] $CODE_FONTFILE [验证码字体]
	 */
	public function __construct($CODE_LEN=NULL,$CODE_WIDTH=NULL,$CODE_HEIGHT=NULL,$CODE_BGCOLOR=NULL,$CODE_COLOR=NULL,$CODE_SIZE=NULL,$CODE_FONTFILE=NULL){

		$this->CODE_LEN      = is_null($CODE_LEN)     ? C('CODE_LEN')      : $CODE_LEN;
		$this->CODE_SIZE     = is_null($CODE_SIZE)    ? C('CODE_SIZE')     : $CODE_SIZE;
		$this->CODE_COLOR    = is_null($CODE_COLOR)   ? C('CODE_COLOR')    : $CODE_COLOR;
		$this->CODE_BGCOLOR  = is_null($CODE_BGCOLOR) ? C('CODE_BGCOLOR')  : $CODE_BGCOLOR;
		$this->CODE_WIDTH    = is_null($CODE_WIDTH)   ? C('CODE_WIDTH')    : $CODE_WIDTH;
		$this->CODE_HEIGHT   = is_null($CODE_HEIGHT)  ? C('CODE_HEIGHT')   : $CODE_HEIGHT;
		$this->CODE_FONTFILE = is_null($CODE_FONTFILE)? C('CODE_FONTFILE') : $CODE_FONTFILE;
		$this->CODE_SEEK     = C('CODE_SEEK');

	}
	public function show(){
		header('Content-Type:image/png');   		//头部
		$this->_mk_img();  							//创建画布
		$this->_fill_color();						//画布填充颜色
		$this->_fill_pixel();						//画布填充干扰素
		$_SESSION['code'] = $this->fill_text();		//画布填充验证码文字.并将验证码写入session;
		imagepng($this->img);						//输出图片
		imagedestroy($this->img);					//关闭图像释放资源
	}

	private function _mk_img(){
		$img = imagecreatetruecolor($this->CODE_WIDTH, $this->CODE_HEIGHT);
		$this->img = $img;
	}

	private function _fill_color(){
		$CODE_BGCOLOR = hexdec($this->CODE_BGCOLOR);
		imagefill($this->img, 0, 0, $CODE_BGCOLOR);
	}

	private function _fill_pixel(){
		for ($i=0; $i < 300; $i++) { 
			$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
			imagesetpixel($this->img, $this->CODE_WIDTH, $this->CODE_HEIGHT, $color);
		}
		for ($i=0; $i < 10; $i++) { 
			$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
			//画直线;
			//imageline($this->img, mt_rand(0,100), mt_rand(0,25), mt_rand(0,100), mt_rand(0,25), $color);
			//画弧线;
			imagearc($this->img, mt_rand(0,100), mt_rand(0,25), mt_rand(0,100), mt_rand(0,25), 75, 170, $color);
		}		
	}

	private function fill_text(){
		$verifyCode = '';
		for ($i=0; $i < $this->CODE_LEN; $i++) { 
			//计算文字间距
			$x = ($this->CODE_WIDTH)/$this->CODE_LEN-3;
			//每个文字的位置
			$x = $x*$i+10;
			//计算基线值
			$y= ($this->CODE_HEIGHT+$this->CODE_SIZE)/2;
			//随机从种子中拿文字
			$seek = $this->CODE_SEEK;
			$text = strtoupper($seek[mt_rand(0,strlen($seek)-1)]);
			$CODE_COLOR = hexdec($this->CODE_COLOR);
			imagettftext($this->img, $this->CODE_SIZE, mt_rand(-45,45), $x, $y,$CODE_COLOR , $this->CODE_FONTFILE, $text);
			$verifyCode .= $text;
		}	
		return $verifyCode;
	}
}
?>
