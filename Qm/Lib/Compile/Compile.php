<?php
/* 
* @Title: [把框架所有需要载入的文件集合起来,便于以后的拓展]
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 21:03:22
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-12 14:50:28
* @Copyright: Hn7m.com
*/

return array(
	
	CORE_PATH . '/SmartyView.class.php',
	CORE_PATH . '/Control.class.php',
	CORE_PATH . '/Application.class.php',
	FUNCTION_PATH.'/function.php'
	);

?>