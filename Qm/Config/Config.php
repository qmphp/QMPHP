<?php
/* 
*[框架配置项]
* @Author: ChenLei [chenleib5@126.com]
* @Date:   2014-02-25 15:30:21
* @Last Modified time: 2014-03-15 23:11:30
*/

//配置项里有一些隐私元素,这里处于安全性考虑,只允许从index单入口接入
if(!defined('QM_PATH')) die('access not allow!');

return array(

	

	/************************验证码配置项**********************/
	'CODE_LEN'			=>	4,
	'CODE_SIZE'			=>	16,
	'CODE_COLOR'		=>	'#ffffff',
	'CODE_BGCOLOR'	    =>	'#000000',
	'CODE_WIDTH'		=>	100,
	'CODE_HEIGHT'		=>	25,
	'CODE_FONTFILE'		=>	DATA_PATH. '/Font/font.ttf',
	'CODE_SEEK'			=>	'abcdefghigklmnopqrstuvwxyz1234567890',

	/*************************缩略图配置项************************/
	'THUMB_WIDTH'		=>	100,
	'THUMB_HEIGHT'		=>	100,
	'THUMB_PREFIX'		=>	'thumb_',

	/*************************水印配置项**************************/
	'WATER_POSTION'		=>	1,
	'WATER_ALPHA'		=>	80,
	'WATER_QUALITY'		=>	80,

	/*************************数据库配置项**************************/
	'DB_HOST'		=>	'',
	'DB_USER'		=>	'',
	'DB_PASSWORD'	=>	'',
	'DB_NAME'		=>	'',
	'DB_CHARSET'	=>	'utf8',
	'DB_PREFIX'		=>	'',

	/*************************是否允许新会员注册*********************/
	'RES_ON'		=>	TRUE,

	/*************************COOKIE有效期*********************/
	'COOKIE_TIME'	=>	time()+3600 * 24 * 7,

	/*************************SMARTY配置*********************/
	'SMARTY_ON'				=>	true,
	'SMARTY_DELIMITER_LEFT'	=>	"{qm:",
	'SMARTY_DELIMITER_RIGHT'=>  "}",
	'SMARTY_CACHE_ON'		=>	false,
	'SMARTY_CACHE_TIME'		=>	60,

	/*************************头像上传配置*********************/
	'UPLOAD_PATH'	=>	UPLOAD_PATH . "/Upload" . date('Ymd'),
	'UPLOAD_SIZE'	=>	5000000,
	'UPLOAD_TYPE'	=>	array('jpg','gif','png','jpeg')
);